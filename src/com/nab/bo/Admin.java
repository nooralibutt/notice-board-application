package com.nab.bo;


import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class Admin {
	
	
	
	public int Aid ;
    public int getAid() {
		return Aid;
	}
	public Admin setAid(int aid) {
		Aid = aid;
		return this;
	}
	public String getUsername() {
		return Username;
	}
	public Admin setUsername(String username) {
		Username = username;
		return this;
	}
	public String getPassword() {
		return Password;
	}
	public Admin setPassword(String password) {
		Password = password;
		return this;
	}
	public String getEmail() {
		return Email;
	}
	public Admin setEmail(String email) {
		Email = email;
		return this;
	}
	public String getName() {
		return Name;
	}
	public Admin setName(String name) {
		Name = name;
		return this;
	}
	public int getContact() {
		return Contact;
	}
	public Admin setContact(int contact) {
		Contact = contact;
		return this;
	}
	public boolean isIsActive() {
		return IsActive;
	}
	public Admin setIsActive(boolean isActive) {
		IsActive = isActive;
		return this;
	}
	public String getDesignation() {
		return Designation;
	}
	public Admin setDesignation(String designation) {
		Designation = designation;
		return this;
	}
	public int getSystemAdmin_Said() {
		return SystemAdmin_Said;
		
	}
	public Admin setSystemAdmin_Said(int systemAdmin_Said) {
		SystemAdmin_Said = systemAdmin_Said;
		return this;
	}
	public String Username ;
    public String Password ;
    public String Email ;
    public String Name ;
    public int Contact ;
    public boolean IsActive ;
    public String Designation ;
    public int SystemAdmin_Said ;
    
    
    public JSONObject getJsonObject() {
		JSONObject jobj = new JSONObject();
		try {
			jobj.put("Username", getUsername());
			jobj.put("Password", getPassword());
			jobj.put("Email", getEmail());
			jobj.put("Name", getName());
			jobj.put("Contact", getContact());
			jobj.put("IsActive", isIsActive());
			jobj.put("Designation", getDesignation());
			jobj.put("Aid", getAid());
			jobj.put("SystemAdmin_Said", getSystemAdmin_Said());
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return jobj;
	}
    public static Admin getNewAdmin(int id, String username, String password, String email,String name, int contact, 
			boolean isactive, String Designation,
			int createdid) {

		return new Admin().setContact(contact).setDesignation(Designation).setEmail(email).setIsActive(isactive).setName(name).setPassword(password).setAid(id).setSystemAdmin_Said(createdid).setUsername(username);

	}
    public void logAdmin() {
		Log.i("getAid", getAid()+"");
		Log.i("getUsername", getUsername()+"");
		Log.i("getPassword", getPassword()+"");
		Log.i("getEmail", getEmail()+"");
		Log.i("getName", getName()+"");
		Log.i("getContact", getContact()+"");
		Log.i("getDesignation", getDesignation()+"");
		Log.i("IsActive", isIsActive()+"");
		Log.i("getSystemAdmin_Said", getSystemAdmin_Said()+"");
		
		
	}
    public static Admin getAdmin(JSONObject obj) {

		Admin s;
		try {
			s = getNewAdmin(obj.getInt("Aid"), obj.getString("Username"),obj.getString("Password"), obj.getString("Email"), obj.getString("Name")
					, obj.getInt("Contact"),obj.getBoolean("IsActive"), obj.getString("Designation"),
					obj.getInt("SystemAdmin_Said"));
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return s;

	}

}