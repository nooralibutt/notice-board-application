package com.nab.bo;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class Student {
	
	
	
	public int Sid ;
    public int getSid() {
		return Sid;
	}
	public Student setSid(int sid) {
		Sid = sid;
		return this;
	}
	public String getUsername() {
		return Username;
	}
	public Student setUsername(String username) {
		Username = username;
		return this;
	}
	public String getPassword() {
		return Password;
	}
	public Student setPassword(String password) {
		Password = password;
		return this;
	}
	public String getEmail() {
		return Email;
	}
	public Student setEmail(String email) {
		Email = email;
		return this;
	}
	public String getName() {
		return Name;
	}
	public Student setName(String name) {
		Name = name;
		return this;
	}
	public int getContact() {
		return Contact;
	}
	public Student setContact(int contact) {
		Contact = contact;
		return this;
	}
	public boolean isIsActive() {
		return IsActive;
	}
	public Student setIsActive(boolean isActive) {
		IsActive = isActive;
		return this;
	}
	public String getDegree() {
		return Degree;
	}
	public Student setDegree(String degree) {
		Degree = degree;
		return this;
	}
	public int getSystemAdmin_Said() {
		return SystemAdmin_Said;
		
	}
	public Student setSystemAdmin_Said(int systemAdmin_Said) {
		SystemAdmin_Said = systemAdmin_Said;
		return this;
	}
	
	public String Username ;
    public String Password ;
    public String Email ;
    public String Name ;
    public int Contact ;
    public boolean IsActive ;
    public String Degree ;
    public int SystemAdmin_Said ;
    
    
    public JSONObject getJsonObject() {
		JSONObject jobj = new JSONObject();
		try {
			jobj.put("Sid", getSid());
			jobj.put("Username", getUsername());
			jobj.put("Password", getPassword());
			jobj.put("Email", getEmail());
			jobj.put("Name", getName());
			jobj.put("Contact", getContact());
			jobj.put("IsActive", isIsActive());
			jobj.put("Degree", getDegree());
			jobj.put("SystemAdmin_Said", getSystemAdmin_Said());
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return jobj;
	}
    public static Student getNewStudent(int id, String username, String password, String email,String name, int contact, 
			boolean isactive, String degree,
			int createdid) {

		return new Student().setContact(contact).setDegree(degree).setEmail(email).setIsActive(isactive).setName(name).setPassword(password).setSid(id).setSystemAdmin_Said(createdid).setUsername(username);
	}
    
    public void logStudent() {
		Log.i("getSid", getSid()+"");
		Log.i("getUsername", getUsername()+"");
		Log.i("getPassword", getPassword()+"");
		Log.i("getEmail", getEmail()+"");
		Log.i("getName", getName()+"");
		Log.i("getContact", getContact()+"");
		Log.i("getDegree", getDegree()+"");
		Log.i("IsActive", isIsActive()+"");
		Log.i("getSystemAdmin_Said", getSystemAdmin_Said()+"");
	}
    public static Student getStudent(JSONObject obj) {

		Student s;
		try {
			s = getNewStudent(obj.getInt("Sid"), obj.getString("Username"),obj.getString("Password"), obj.getString("Email"), obj.getString("Name")
					, obj.getInt("Contact"),obj.getBoolean("IsActive"), obj.getString("Degree"),
					obj.getInt("SystemAdmin_Said"));
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return s;

	}

}