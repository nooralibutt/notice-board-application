package com.nab.bo;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class Notice {
	private int Nid;
	private String Text;
	private boolean IsPublished;
	private String CreatedAt;
	private boolean IsRemoved;

	private String CreatedBy;
	private String CreatedID;
	private String Title;
	private String ContentType;

	private Favourite favourite;
	private List<Comment> commentList;
	private List<Integer> favouriteList;

	public List<Integer> getFavouriteList() {
		return favouriteList;
	}

	public Notice setFavouriteList(List<Integer> favouriteList2) {
		this.favouriteList = favouriteList2;
		return this;
	}

	public Favourite getFavourite() {
		return favourite;
	}

	public Notice setFavourite(Favourite favourite) {
		this.favourite = favourite;
		return this;
	}

	public String getContentType() {
		return ContentType;
	}

	public Notice setContentType(String contentType) {
		ContentType = contentType;
		return this;
	}

	public boolean isTeacherNotice() {
		return isTeacherNotice;
	}

	public Notice setTeacherNotice(boolean isTeacherNotice) {
		this.isTeacherNotice = isTeacherNotice;
		return this;
	}

	private boolean isTeacherNotice;

	public String getTitle() {
		return Title;
	}

	public Notice setTitle(String title) {
		Title = title;
		return this;
	}

	public JSONObject getJsonObject() {
		JSONObject jobj = new JSONObject();
		try {
			jobj.put("Text", getText());
			jobj.put("IsPublished", isIsPublished());
			jobj.put("CreatedAt", getCreatedAt());
			jobj.put("IsRemoved", isIsRemoved());
			jobj.put("CreatedBy", getCreatedBy());
			jobj.put("CreatedID", getCreatedID());
			jobj.put("isTeacherNotice", isTeacherNotice());
			jobj.put("Title", getTitle());
			jobj.put("ContentType", getContentType());
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return jobj;
	}

	public void logNotice() {
		Log.i("getNid", getNid() + "");
		Log.i("getCreatedAt", getCreatedAt() + "");
		Log.i("getCreatedBy", getCreatedBy() + "");
		Log.i("getCreatedID", getCreatedID() + "");
		Log.i("getText", getText() + "");
		Log.i("isIsPublished", isIsPublished() + "");
		Log.i("isIsRemoved", isIsRemoved() + "");
		Log.i("ContentType", getContentType() + "");
		Log.i("Title", getTitle() + "");
		Log.i("isTeacherNotice", isTeacherNotice() + "");

	}

	public static Notice getNotice(JSONObject obj) {

		Notice n;
		try {

			JSONArray jsonArray = obj.getJSONArray("Comments");
			List<Comment> commentList = new ArrayList<Comment>();

			for (int i = 0; i < jsonArray.length(); i++) {
				Comment comment2 = Comment.getComment(jsonArray
						.getJSONObject(i));
				commentList.add(comment2);
			}

			jsonArray = obj.getJSONArray("Favourites");
			List<Integer> favouriteList = new ArrayList<Integer>();

			for (int i = 0; i < jsonArray.length(); i++) {
				Integer sid = jsonArray.getJSONObject(i).getInt("StudentSid");
				favouriteList.add(sid);

			}

			n = getNewNotice(obj.getInt("Nid"), obj.getString("Text"),
					obj.getBoolean("IsPublished"), obj.getString("CreatedAt"),
					obj.getBoolean("IsRemoved"), obj.getString("CreatedBy"),
					obj.getString("CreatedID"), obj.getString("Title"),
					obj.getString("ContentType"), obj.getBoolean("isTeacherNotice"));
			n.setCommentList(commentList).setFavouriteList(favouriteList);

		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return n;

	}

	public static Notice getNewNotice(int id, String text, boolean ispublish,
			String createat, boolean isremove, String createdby,
			String createdid, String title, String contentType,
			boolean isTeachNot) {

		return new Notice().setCreatedBy(createdby).setCreatedID(createdid)
				.setNid(id).setCreatedAt(createat).setText(text)
				.setIsPublished(ispublish).setIsRemoved(isremove)
				.setContentType(contentType).setTitle(title)
				.setTeacherNotice(isTeachNot);
	}

	public int getNid() {
		return Nid;
	}

	public Notice setNid(int nid) {
		Nid = nid;
		return this;
	}

	public String getText() {
		return Text;
	}

	public Notice setText(String text) {
		Text = text;
		return this;
	}

	public boolean isIsPublished() {
		return IsPublished;
	}

	public Notice setIsPublished(boolean isPublished) {
		IsPublished = isPublished;
		return this;
	}

	public String getCreatedAt() {
		return CreatedAt;
	}

	public Notice setCreatedAt(String createdAt) {
		CreatedAt = createdAt;
		return this;
	}

	public boolean isIsRemoved() {
		return IsRemoved;
	}

	public Notice setIsRemoved(boolean isRemoved) {
		IsRemoved = isRemoved;
		return this;
	}

	public String getCreatedBy() {
		return CreatedBy;
	}

	public Notice setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
		return this;
	}

	public String getCreatedID() {
		return CreatedID;
	}

	public Notice setCreatedID(String createdID) {
		CreatedID = createdID;
		return this;
	}

	public List<Comment> getCommentList() {
		return commentList;
	}

	public Notice setCommentList(List<Comment> commentList) {
		this.commentList = commentList;
		return this;
	}

	@Override
	public String toString() {
		return getTitle();
	}
}
