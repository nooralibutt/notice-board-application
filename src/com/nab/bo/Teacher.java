package com.nab.bo;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class Teacher {
	
	
	
	public int Tid ;
    public int getTid() {
		return Tid;
	}
	public Teacher setTid(int tid) {
		Tid = tid;
		return this;
	}
	public String getUsername() {
		return Username;
	}
	public Teacher setUsername(String username) {
		Username = username;
		return this;
	}
	public String getPassword() {
		return Password;
	}
	public Teacher setPassword(String password) {
		Password = password;
		return this;
	}
	public String getEmail() {
		return Email;
	}
	public Teacher setEmail(String email) {
		Email = email;
		return this;
	}
	public String getName() {
		return Name;
	}
	public Teacher setName(String name) {
		Name = name;
		return this;
	}
	public int getContact() {
		return Contact;
	}
	public Teacher setContact(int contact) {
		Contact = contact;
		return this;
	}
	public boolean isIsActive() {
		return IsActive;
	}
	public Teacher setIsActive(boolean isActive) {
		IsActive = isActive;
		return this;
	}
	public String getType() {
		return Type;
	}
	public Teacher setType(String type) {
		Type = type;
		return this;
	}
	public int getSystemAdmin_Said() {
		return SystemAdmin_Said;
		
	}
	public Teacher setSystemAdmin_Said(int systemAdmin_Said) {
		SystemAdmin_Said = systemAdmin_Said;
		return this;
	}
	public String Username ;
    public String Password ;
    public String Email ;
    public String Name ;
    public int Contact ;
    public boolean IsActive ;
    public String Type ;
    public int SystemAdmin_Said ;
    
    
    public JSONObject getJsonObject() {
		JSONObject jobj = new JSONObject();
		try {
			jobj.put("Username", getUsername());
			jobj.put("Password", getPassword());
			jobj.put("Email", getEmail());
			jobj.put("Name", getName());
			jobj.put("Contact", getContact());
			jobj.put("IsActive", isIsActive());
			jobj.put("Type", getType());
			jobj.put("Tid", getTid());
			jobj.put("SystemAdmin_Said", getSystemAdmin_Said());
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return jobj;
	}
    public static Teacher getNewTeacher(int id, String username, String password, String email,String name, int contact, 
			boolean isactive, String type,
			int createdid) {

		return new Teacher().setContact(contact).setType(type).setEmail(email).setIsActive(isactive).setName(name).setPassword(password).setTid(id).setSystemAdmin_Said(createdid).setUsername(username);

	}
    public void logTeacher() {
		Log.i("getTid", getTid()+"");
		Log.i("getUsername", getUsername()+"");
		Log.i("getPassword", getPassword()+"");
		Log.i("getEmail", getEmail()+"");
		Log.i("getName", getName()+"");
		Log.i("getContact", getContact()+"");
		Log.i("getType", getType()+"");
		Log.i("IsActive", isIsActive()+"");
		Log.i("getSystemAdmin_Said", getSystemAdmin_Said()+"");
		
		
	}
    public static Teacher getTeacher(JSONObject obj) {

		Teacher s;
		try {
			s = getNewTeacher(obj.getInt("Tid"), obj.getString("Username"),obj.getString("Password"), obj.getString("Email"), obj.getString("Name")
					, obj.getInt("Contact"),obj.getBoolean("IsActive"), obj.getString("Type"),
					obj.getInt("SystemAdmin_Said"));
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return s;

	}

}