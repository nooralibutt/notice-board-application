package com.nab.bo;

import org.json.JSONException;
import org.json.JSONObject;

public class Favourite {

	public int getFid() {
		return Fid;
	}

	public Favourite setFid(int fid) {
		Fid = fid;
		return this;
	}

	public String getFavTime() {
		return FavTime;
	}

	public Favourite setFavTime(String favTime) {
		FavTime = favTime;
		return this;
	}

	public int getStudentSid() {
		return StudentSid;
	}

	public Favourite setStudentSid(int studentSid) {
		StudentSid = studentSid;
		return this;
	}

	public int getNoticeNid() {
		return NoticeNid;
	}

	public Favourite setNoticeNid(int noticeNid) {
		NoticeNid = noticeNid;
		return this;
	}

	public Student getStudent() {
		return student;
	}

	public Favourite setStudent(Student student) {
		this.student = student;
		return this;
	}

	public int Fid;
	public String FavTime;
	public int StudentSid;
	public int NoticeNid;
	public Student student;

	public static Favourite getNewFavourite(int fid, String favtime,
			int studentsid, int noticenid) {

		return new Favourite().setFid(fid).setFavTime(favtime)
				.setNoticeNid(noticenid).setStudentSid(studentsid);
	}

	public static Favourite getFavourite(JSONObject obj) {

		Favourite f;
		try {
			Student stud = Student.getStudent(obj.getJSONObject("Student"));
			f = getNewFavourite(obj.getInt("Fid"), obj.getString("FavTime"),
					obj.getInt("StudentSid"), obj.getInt("NoticeNid"));
			f.setStudent(stud);
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return f;

	}

	public JSONObject getJsonObject() {
		JSONObject jobj = new JSONObject();
		try {
			jobj.put("Fid", getFid());
			jobj.put("FavTime", getFavTime());
			jobj.put("StudentSid", getStudentSid());
			jobj.put("NoticeNid", getNoticeNid());

		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return jobj;
	}

}
