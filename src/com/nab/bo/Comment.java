package com.nab.bo;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class Comment {
	
	
    public String getRemarks() {
		return Remarks;
	}
	public Comment setRemarks(String remarks) {
		Remarks = remarks;
		return this;
	}
	public String getPostedOn() {
		return PostedOn;
	}
	public Comment setPostedOn(String postedOn) {
		PostedOn = postedOn;
		return this;
	}
	public int getCid() {
		return Cid;
	}
	public Comment setCid(int cid) {
		Cid = cid;
		return this;
	}
	public int getNoticeNid() {
		return NoticeNid;
	}
	public Comment setNoticeNid(int noticeNid) {
		NoticeNid = noticeNid;
		return this;
	}
	public int getStudentSid() {
		return StudentSid;
	}
	public Comment setStudentSid(int studentSid) {
		StudentSid = studentSid;
		return this;
	}
	  
		
	public String Remarks ;
	public String PostedOn ;   
    public int Cid ;
    public int NoticeNid ;
    public int StudentSid ;    
    public Student student;
    
    
    
    public Student getStudent() {
		return student;
	}
	public Comment setStudent(Student student) {
		this.student = student;
		return this;
	}
	public static Comment getComment(JSONObject obj) {

		Comment c;
		try {
			Student stud = Student.getStudent(obj.getJSONObject("Student"));
			c = getNewComment(obj.getInt("Cid"), obj.getString("Remarks"),obj.getString("PostedOn"),obj.getInt("NoticeNid") , obj.getInt("StudentSid"));
			c.setStudent(stud);
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return c;

	}
    public void logComment() {
		
		Log.i("Remarks", getRemarks()+"");
		Log.i("PostedOn", getPostedOn()+"");
		Log.i("Cid", getCid()+"");
		Log.i("NoticeNid", getNoticeNid()+"");
		Log.i("StudentSid", getStudentSid()+"");
		
		
	}
    
    public static Comment getNewComment(int cid, String remarks, String postedon, int notnid, int stdid) {

		return new Comment().setCid(cid).setRemarks(remarks).setPostedOn(postedon).setNoticeNid(notnid).setStudentSid(stdid);
	}
    public JSONObject getJsonObject() {
		JSONObject jobj = new JSONObject();
		try {
			jobj.put("Remarks", getRemarks());
			jobj.put("PostedOn", getPostedOn());
			jobj.put("Cid", getCid());
			jobj.put("NoticeNid", getNoticeNid());
			jobj.put("StudentSid", getStudentSid());
			
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return jobj;
	}
    
}
