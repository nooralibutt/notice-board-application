package com.nab.webservices;

import com.nab.utils.Utils;

public class Constants {
	public static final String WEB_URL_NOTICE = "http://pucitnoticeboard.apphb.com/api/notice";
	public static final String WEB_URL_COMMENT = "http://pucitnoticeboard.apphb.com/api/comment";
	public static final String WEB_URL_STUDENT = "http://pucitnoticeboard.apphb.com/api/student";
	public static final String WEB_URL_TECACHER = "http://pucitnoticeboard.apphb.com/api/teacher";
	public static final String WEB_URL_ADMIN = "http://pucitnoticeboard.apphb.com/api/admin";

	public static String getLoginURL() {

		String url = null;

		switch (Utils.selectedUser) {
		case Student:
			url = WEB_URL_STUDENT;
			break;
		case Teacher:
			url = WEB_URL_TECACHER;
			break;
		case Admin:
			url = WEB_URL_ADMIN;
			break;
		}

		return url;
	}

}
