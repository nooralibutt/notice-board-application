package com.nab.webservices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.fyp.NoticeListActivity;
import com.nab.utils.Utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class Login extends AsyncTask<String, Void, Void> {

	private int statusCode;
	
	ProgressDialog progressDialog = null;
	Context context;
	
	public Login(Context c){
		context = c;
		progressDialog = new ProgressDialog(context);
		progressDialog.setCancelable(false);
		progressDialog.setIndeterminate(true);
		progressDialog.setMessage("Logging you in ...");
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		
		progressDialog.show();
	}
	
	@Override
	protected Void doInBackground(String... arg0) {
		HttpClient client = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000);
		HttpResponse response = null;
		HttpGet httpGet;

		try {
			httpGet = new HttpGet(Constants.getLoginURL() + "?" + 
					"username=" + URLEncoder.encode(arg0[0], "UTF-8") + "&" + 
					"password=" + URLEncoder.encode(arg0[1], "UTF-8") 
					);
			
			response = client.execute(httpGet);
			statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == 200) {
				InputStream inStream = null;
				inStream = response.getEntity().getContent();
				String line;
				StringBuilder builder = new StringBuilder();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(inStream));

				while ((line = reader.readLine()) != null) {
					builder.append(line);

				}
				String jsonString = builder.toString();
				try {
					JSONObject user = new JSONObject(jsonString);
					Log.i("Login response: ", user.toString());
					Utils.setPerson(user);			
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			else
				Log.e("Login", "Login failure");
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		
		progressDialog.dismiss();
		
		if(statusCode == 200){
			
			Toast.makeText(context,"Signed In", Toast.LENGTH_LONG).show();
			
			Intent i = new Intent(context , NoticeListActivity.class);
			context.startActivity(i);
			((Activity) context).finish();
		}
		else
			Toast.makeText(context, "Invalid username or password", Toast.LENGTH_SHORT).show();
	}

}