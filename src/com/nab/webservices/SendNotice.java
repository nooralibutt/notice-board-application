package com.nab.webservices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import com.nab.bo.Notice;

import android.os.AsyncTask;
import android.util.Log;

public class SendNotice extends AsyncTask<Void, Void, Void> {

	Notice notice;
	
	public SendNotice(Notice _notice) {
		notice = _notice;
	}

	private int statusCode;

	@Override
	protected void onPostExecute(Void result) {
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	@Override
	protected Void doInBackground(Void... arg0) {
		try {
			
			JSONObject jsonObj = notice.getJsonObject();
			
			HttpClient httpclient = new DefaultHttpClient();
			HttpConnectionParams.setConnectionTimeout(httpclient.getParams(), 10000);
			
			HttpPost httpPost = new HttpPost(Constants.WEB_URL_NOTICE);

			StringEntity entity = new StringEntity(jsonObj.toString());
			entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
					"application/json"));
			entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE,
					"application/json"));

			httpPost.setEntity(entity);
			httpPost.setHeader("Content-type", "application/json");

			HttpResponse httpResponse = httpclient.execute(httpPost);
			statusCode = httpResponse.getStatusLine().getStatusCode();

			InputStream inStream = null;
			inStream = httpResponse.getEntity().getContent();
			String line;
			StringBuilder builder = new StringBuilder();
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(inStream));

			while ((line = reader.readLine()) != null) {
				builder.append(line);

			}
			String jsonString = builder.toString();
			
			Log.i("Notice posting response: ", jsonString);

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (statusCode == 201) {
			Log.i("Notice posted: ", "Success");
		} else {
			Log.i("Notice posting failure with status code: ", statusCode + "");
		}

		return null;

	}

}