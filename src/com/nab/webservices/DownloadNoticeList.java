package com.nab.webservices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.fyp.NoticeListActivity;
import com.example.fyp.R;
import com.nab.bo.Notice;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.ArrayAdapter;
import android.widget.Toast;

public class DownloadNoticeList extends AsyncTask<Void, Void, Void> {

	private int statusCode;
	List<Notice> noticeList = null;
	
	ProgressDialog progressDialog = null;

	private NoticeListActivity noticeListActivity;
	
	public DownloadNoticeList(NoticeListActivity _noticeListActivity){
		
		noticeListActivity = _noticeListActivity;
		
		progressDialog = new ProgressDialog(noticeListActivity);
		progressDialog.setCancelable(false);
		progressDialog.setIndeterminate(true);
		progressDialog.setMessage("updating notices ...");
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		
		progressDialog.show();
	}
	
	@Override
	protected Void doInBackground(Void... arg0) {
		HttpClient client = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000);
		HttpResponse response = null;
		HttpGet httpGet = new HttpGet(Constants.WEB_URL_NOTICE);

		try {
			response = client.execute(httpGet);
			statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == 200) {
				InputStream inStream = null;
				inStream = response.getEntity().getContent();
				String line;
				StringBuilder builder = new StringBuilder();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(inStream));

				while ((line = reader.readLine()) != null) {
					builder.append(line);

				}
				String jsonString = builder.toString();
				
				try {
					
					noticeList = new ArrayList<Notice>();
					
					//building array list of notices
					JSONArray noticesArray = new JSONArray(jsonString);
					for (int i = 0; i < noticesArray.length(); i++) {
						JSONObject object = noticesArray.getJSONObject(i);
						Notice notice = Notice.getNotice(object);
						noticeList.add(notice);
						notice.logNotice();
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}

			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;

	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		
		progressDialog.dismiss();
		
		if(statusCode == 200){
			
			//Initializing notice list
			NoticeListActivity.noticeList = noticeList;
			
			ArrayAdapter<Notice> adapter =
					new ArrayAdapter<Notice>(noticeListActivity, R.layout.list_item_layout, noticeList);
			noticeListActivity.setListAdapter(adapter);
		}
		else
			Toast.makeText(noticeListActivity, "Couldn't update list", Toast.LENGTH_SHORT).show();
	}
}
