package com.nab.utils;

import org.json.JSONObject;

import com.nab.bo.Admin;
import com.nab.bo.Student;
import com.nab.bo.Teacher;

public class Utils {
	public enum UserType {
		Student, Admin, Teacher
	};

	public static UserType selectedUser = UserType.Student;

	public static Student student = null;
	public static Teacher teacher = null;
	public static Admin admin = null;

	public static void setPerson(JSONObject json) {

		switch (Utils.selectedUser) {
		case Student:
			student = Student.getStudent(json);
			break;
		case Teacher:
			teacher = Teacher.getTeacher(json);
			break;
		case Admin:
			admin = Admin.getAdmin(json);
			break;
		}

	}
}
