package com.example.fyp;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Home_Page extends Activity {
	
	AlertDialog.Builder dialogue;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home__page);
		

		Button b =  (Button) findViewById(R.id.button3);
		b.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				Intent i = new Intent(Home_Page.this , CreatePerson.class);
				startActivity(i);
			}
		});
		
		Button b1 =  (Button) findViewById(R.id.button1);
		b1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				Intent i = new Intent(Home_Page.this , SignIn.class);
				startActivity(i);
				
				Toast.makeText(Home_Page.this,"Logged Out", Toast.LENGTH_LONG).show();
			}
		});
		
		Button b2 =  (Button) findViewById(R.id.button2);
		b2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				dialogue = new AlertDialog.Builder ( Home_Page.this );
				dialogue.setTitle("Deactivate");
				dialogue.setMessage("Are you sure you want to deactivate?");
				dialogue.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						Toast.makeText(Home_Page.this,"Successfully deactivated.", Toast.LENGTH_LONG).show();
						Intent i = new Intent( Home_Page.this , SignIn.class);
						startActivity(i);
						
					}
				});
					
				dialogue.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						//Toast.makeText(Home_Page.this,"No clicked", Toast.LENGTH_LONG).show();
						
					}
				});
				
				//Toast.makeText(Home_Page.this,"Deactivated", Toast.LENGTH_LONG).show();
			
			
			dialogue.show ();
			}
		
		});
		
		TextView t =  (TextView) findViewById(R.id.textView3);
		t.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				Intent i = new Intent(Home_Page.this , ViewNoticeActivity.class);
				startActivity(i);
				
				
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
	
		return true;
	}

}
