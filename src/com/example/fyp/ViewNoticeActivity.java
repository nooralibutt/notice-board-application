package com.example.fyp;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.nab.bo.Comment;
import com.nab.bo.Notice;
import com.nab.utils.Utils;
import com.nab.webservices.PostComment;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class ViewNoticeActivity extends Activity {

	CommentsDialog commentDialogue;
	Notice currentNotice;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_notice);

		int position = getIntent().getExtras().getInt(
				NoticeListActivity.EXTRA_SELECTED_NOTICE);
		currentNotice = NoticeListActivity.noticeList.get(position);

		TextView tv_title_vn = (TextView) findViewById(R.id.tv_title_vn);
		tv_title_vn.setText(currentNotice.getTitle());

		TextView tv_text_vn = (TextView) findViewById(R.id.tv_text_vn);
		tv_text_vn.setText(currentNotice.getText());

		TextView tv_createdby_vn = (TextView) findViewById(R.id.tv_createdby_vn);
		tv_createdby_vn.setText(currentNotice.getCreatedBy());

		TextView tv_date_vn = (TextView) findViewById(R.id.tv_date_vn);
		tv_date_vn.setText(currentNotice.getCreatedAt());
	}

	public void onComment(View arg0) {
		commentDialogue = new CommentsDialog(ViewNoticeActivity.this,
					currentNotice.getCommentList(), currentNotice);
		commentDialogue.show(getFragmentManager(), "listDialogFragment");
	}

	public void onFavorite(View arg0) {
		ImageButton img = (ImageButton) findViewById(R.id.ib_favorite_vn);
		img.setImageResource(R.drawable.favoritefinal2);

		// do coding for making current notice favorite of current user

		Toast.makeText(ViewNoticeActivity.this, "Notice marked as favorite",
				Toast.LENGTH_LONG).show();

	}

	public void onShare(View arg0) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {

				Intent shareIntent = new Intent(
						android.content.Intent.ACTION_SEND);
				shareIntent.setType("text/plain");
				shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
						currentNotice.getTitle());
				shareIntent.putExtra(android.content.Intent.EXTRA_TEXT,
						currentNotice.getText());
				shareIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
						"hasni_90@hotmail.com");
				PackageManager pm = getApplicationContext().getPackageManager();
				List<ResolveInfo> activityList = pm.queryIntentActivities(
						shareIntent, 0);
				for (final ResolveInfo app : activityList) {
					if ((app.activityInfo.name).contains("gmail")) {
						final ActivityInfo activity = app.activityInfo;
						final ComponentName name = new ComponentName(
								activity.applicationInfo.packageName,
								activity.name);
						shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
						shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
								| Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
						shareIntent.setComponent(name);
						startActivity(shareIntent);
					}
				}
			}
		});
	}
}

class CommentsDialog extends DialogFragment implements
		android.content.DialogInterface.OnClickListener {

	private Context context;
	private List<Comment> commentList;
	private EditText etComment;
	private Notice currentNotice;

	public CommentsDialog(Context c, List<Comment> commentList, Notice notice) {

		context = c;
		this.commentList = commentList;
		currentNotice = notice;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
		adb.setIcon(R.drawable.commentfinal);
		adb.setTitle("Notice Comments");

		CharSequence[] comments = null;
		if (commentList.size() == 0) {
			comments = new CharSequence[1];
			comments[0] = Html.fromHtml("<i>No comments</i>");
		} else {
			comments = new CharSequence[commentList.size()];
			for (int i = 0; i < commentList.size(); i++) {

				Comment comment = commentList.get(i);
				comments[i] = Html.fromHtml("<b>"
						+ comment.getStudent().getName() + ":</b>" + " <small>"
						+ comment.getRemarks() + "</small><br /><sub>"
						+ comment.getPostedOn() + "</sub>");
			}
		}
		adb.setItems(comments, null);

		if(Utils.selectedUser == Utils.UserType.Student){
			etComment = new EditText(context);
			etComment.setHint("comment...");
	
			adb.setView(etComment);
	
			adb.setPositiveButton("Send", this);
		}
		return adb.create();
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		if (etComment.length() != 0) {
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			
			Comment newComment = Comment.getNewComment(-1, etComment.getText().toString(), dateFormat.format(date), 
					currentNotice.getNid(), Utils.student.getSid());
			
			PostComment postComment = new PostComment(context, currentNotice);
			postComment.execute(newComment);
		}
	}
}