package com.example.fyp;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.nab.utils.Utils;
import com.nab.utils.Utils.UserType;
import com.nab.webservices.Login;

public class SignIn extends Activity implements OnItemSelectedListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_in);

		Spinner spinner = (Spinner) findViewById(R.id.spinner_usertype);
		spinner.setOnItemSelectedListener(this);

		Button b = (Button) findViewById(R.id.button1);
		b.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				EditText et = (EditText) findViewById(R.id.editText2);

				String username = et.getText().toString();

				EditText et2 = (EditText) findViewById(R.id.editText1);
				String password = et2.getText().toString();

				Login login = new Login(SignIn.this);
				login.execute(username, password);

			}

		});

	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		switch (arg2) {
		case 0:
			Utils.selectedUser = UserType.Student;
			break;
		case 1:
			Utils.selectedUser = UserType.Teacher;
			break;
		case 2:
			Utils.selectedUser = UserType.Admin;
			break;
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
	}

}
