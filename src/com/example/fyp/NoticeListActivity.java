package com.example.fyp;

import java.util.List;

import com.example.fyp.R;
import com.nab.bo.Notice;
import com.nab.webservices.DownloadNoticeList;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;

public class NoticeListActivity extends ListActivity {

	public static final String EXTRA_SELECTED_NOTICE = "NOTICE_POSITION";
	public static List<Notice> noticeList = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_all_notices);

		refreshDisplay();
//		 registerForContextMenu(getListView());
	}

	private void refreshDisplay() {
		DownloadNoticeList downloadNoticeList = new DownloadNoticeList(this);		
		downloadNoticeList.execute();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	// @Override
	// public boolean onOptionsItemSelected(MenuItem item) {
	//
	// if (item.getItemId() == R.id.action_create) {
	// createNote();
	// }
	//
	// return super.onOptionsItemSelected(item);
	// }

	// private void createNote() {
	// NoteItem note = NoteItem.getNewItem();
	// Intent intent = new Intent(this, NoteEditorActivity.class);
	// intent.putExtra(NoteItem.EXTRA_KEY, note.getKey());
	// intent.putExtra(NoteItem.EXTRA_TEXT, note.getText());
	// startActivityForResult(intent, EDITOR_ACTIVITY_REQUEST);
	// }
	
	 @Override
	 protected void onListItemClick(ListView l, View v, int position, long id)
	 {
		 Intent intent = new Intent(NoticeListActivity.this, ViewNoticeActivity.class);
		 intent.putExtra(EXTRA_SELECTED_NOTICE, position);
		 startActivity(intent);
	 }

	// @Override
	// protected void onActivityResult(int requestCode, int resultCode, Intent
	// data) {
	// if (requestCode == EDITOR_ACTIVITY_REQUEST && resultCode == RESULT_OK) {
	// NoteItem note = new NoteItem();
	// note.setKey(data.getStringExtra(NoteItem.EXTRA_KEY));
	// note.setText(data.getStringExtra(NoteItem.EXTRA_TEXT));
	// datasource.updateNote(note);
	// refreshDisplay();
	// } else if(requestCode == EDITOR_ACTIVITY_REQUEST && resultCode ==
	// RESULT_CANCELED){
	// Toast.makeText(this, "Note editing cancelled",
	// Toast.LENGTH_SHORT).show();
	// }
	// }

	// //adding delete dialog when user hold press on any note
	// @Override
	// public void onCreateContextMenu(ContextMenu menu, View v,
	// ContextMenuInfo menuInfo) {
	//
	// AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
	// currentNoteId = (int)info.id;
	// menu.add(0, MENU_DELETE_ID, 0, "Delete");
	// }

	// //when user hold press on any note, this method called
	// @Override
	// public boolean onContextItemSelected(android.view.MenuItem item) {
	//
	// if (item.getItemId() == MENU_DELETE_ID) {
	// NoteItem note = notesList.get(currentNoteId);
	// datasource.removeNote(note);
	// refreshDisplay();
	// }
	//
	// return super.onContextItemSelected(item);
	// }
}
